﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LineSwapper : MonoBehaviour
{
    [SerializeField]
    private string[] lines;
    [SerializeField]
    private Button button;
    private Text textContainer;
    private Animator animator;

    public float characterDuration;

    private void Awake()
    {
        textContainer = GetComponent<Text>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        StartCoroutine(Swap());
    }

    private IEnumerator Swap()
    {
        foreach (string line in lines)
        {
            textContainer.text = line;
            yield return new WaitForSeconds(3f);
            yield return new WaitForSeconds(line.Length * characterDuration);
            animator.SetTrigger("Hide");
            yield return new WaitForSeconds(3f);
            animator.SetTrigger("Show");
        }
        textContainer.gameObject.SetActive(false);
        button.gameObject.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    public float speed;
    public Slider staminaLeft;
    public Image fill;  // assign in the editor the "Fill"
    public float maxStamina;
    public float curStamina;
    public float staminaDepletion;
    public string sceneName;
    public GameObject deathPanel;

    private bool isMoving = true;
    private bool isFlipped;
    private Vector3 move;
    private Rigidbody playerRigidBody;
    private CharacterController playerCharacterController;
    private LocationManager locationManager;
    public Camera mainCamera;

    private void Start()
    {
        deathPanel.SetActive(false);
        staminaLeft.maxValue = maxStamina;
        staminaLeft.value = curStamina;
        playerCharacterController = GetComponent<CharacterController>();
        fill.color = new Color(1 - (curStamina / maxStamina), curStamina / maxStamina, 0);
        locationManager = GetComponent<LocationManager>();
        mainCamera = GetComponentInChildren<Camera>();
    }

    private void Update()
    {
        move = new Vector3(Input.GetAxisRaw("Horizontal"),0,Input.GetAxisRaw("Vertical"));

        // Is actually moving?
        if (curStamina > 0)
        {
            if (isMoving && move != Vector3.zero)
            {
                //mainCamera.transform.localPosition += new Vector3(0, 0, -1);

                curStamina -= staminaDepletion * Time.deltaTime;

                if ((move[0] > 0 && !isFlipped) || (move[0] < 0 && isFlipped))
                {
                    transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
                    isFlipped = !isFlipped;
                }

                playerCharacterController.Move(move * speed * Time.deltaTime);
            }
        }

        else
        {
            deathPanel.SetActive(true);
            // When you're out of stamina
            locationManager.ResetLocations();
        }

        staminaLeft.value = curStamina;

        fill.color = new Color(1 - (curStamina / maxStamina), curStamina / maxStamina, 0);
    }

    public void Rest(float missingStaminaPortionToRegen)
    {
        curStamina += (maxStamina - curStamina) * missingStaminaPortionToRegen;
    }

    public void GainLocation()
    {
        mainCamera.transform.localPosition = mainCamera.transform.localPosition + new Vector3(0,0, - 1);
    }

    public void RestartDay()
    {
        Debug.Log("Restarted day at " + gameObject.transform.name);
        deathPanel.SetActive(false);
        gameObject.transform.position = new Vector3(3f, transform.position.y, -1f);
        Rest(1f);
    }

    public bool IsMoving() => isMoving;
    public void SetMoving(bool isMoving) => this.isMoving = isMoving;


}

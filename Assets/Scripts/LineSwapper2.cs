﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LineSwapper2 : MonoBehaviour
{
    [SerializeField]
    private string[] lines;
    [SerializeField]
    private Animator animatorFade;
    private Text textContainer;
    [SerializeField]
    private Credits creditsContainer;
    private Animator animator;
    

    private void Awake()
    {
        textContainer = GetComponent<Text>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        StartCoroutine(Swap());
    }

    private IEnumerator Swap()
    {
        foreach (string line in lines)
        {
            textContainer.text = line;
            yield return new WaitForSeconds(3f);
            yield return new WaitForSeconds(line.Length * 0.06f);
            animator.SetTrigger("Hide");
            yield return new WaitForSeconds(3f);
            animator.SetTrigger("Show");
        }
        textContainer.gameObject.SetActive(false);
        animatorFade.SetTrigger("Fade");
        creditsContainer.Roll();
    }
}

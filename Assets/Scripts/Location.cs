﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Location : MonoBehaviour
{
    [SerializeField]
    private LocationManager locationManager;
    [SerializeField]
    private PlayerMovement playerMovement;
    [SerializeField]
    private Location[] dependentLocations;

    public float restRecovery = 0.1f;
    private Button enterButton;
    private Conversation conversation;
    private Canvas canvas;
    private bool isVisited;
    private bool isDiscovered;


    public void SetVisited()
    {
        if (!isDiscovered)
        {
            isDiscovered = true;
            playerMovement.GainLocation();
        }

        bool isUnlocked = true;
        foreach (Location location in dependentLocations)
        {
            if (!location.IsVisited())
            {
                isUnlocked = false;
                break;
            }
        }
        if (isUnlocked)
        {
            isVisited = true;
            conversation.NextConversation();
        }
    }

    public bool IsVisited()
    {
        return isVisited;
    }

    public Conversation GetConversation()
    {
        return conversation;
    }

    private void Start()
    {
        canvas = GetComponentInChildren<Canvas>();
        enterButton = canvas.GetComponentInChildren<Button>(true);
        conversation = canvas.GetComponentInChildren<Conversation>(true);
        enterButton.onClick.AddListener(EnterConversation);
        enterButton.onClick.AddListener(Rest);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            enterButton.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            enterButton.gameObject.SetActive(false);
        }
        if (conversation.gameObject.activeSelf)
        {
            conversation.gameObject.SetActive(false);
        }
        if (conversation.IsFinished())
        {
            locationManager.MarkVisited(this);
        }
    }

    private void EnterConversation()
    {
        enterButton.gameObject.SetActive(false);
        conversation.gameObject.SetActive(true);
        playerMovement.SetMoving(false);
    }

    private void Rest()
    {
        if (isVisited)
        {
            playerMovement.Rest(restRecovery);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [SerializeField]
    private AudioSource goodSound;
    [SerializeField]
    private AudioSource badSound;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        Instantiate(goodSound);
        Instantiate(badSound);
    }
}

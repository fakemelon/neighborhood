﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// IMPORTANT - inserting conversation content has to be in the following order: static, random, unlockable
public class Conversation : MonoBehaviour
{
    [Serializable]
    private class StringArray
    {
        [SerializeField]
        public string[] part;
        public int Length => part.Length;
    }

    [Serializable]
    private class Pair
    {
        [SerializeField]
        public int index;
        [SerializeField]
        public Location location;
    }

    [SerializeField]
    private StringArray[] parts;
    [SerializeField]
    private List<Pair> dependentLocations;
    [SerializeField]
    private int staticConversatios = 1;
    [SerializeField]
    private int randomConversations = 1;
    [SerializeField]
    private Text textHolder;
    [SerializeField]
    private PlayerMovement playerMovement;

    private int currentConversation = 0;
    private int currentPart = -1;
    private Coroutine printCoroutine;

    private void Awake()
    {
        textHolder.text = "";
    }

    private void Start()
    {
        OnSpacePressed();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnSpacePressed();
        }
    }

    private void OnSpacePressed()
    {
        if (printCoroutine != null)
        {
            StopCoroutine(printCoroutine);
            textHolder.text = parts[currentConversation].part[currentPart];
            printCoroutine = null;
        }
        else if (currentPart + 1 < parts[currentConversation].Length)
        {
            currentPart++;
            printCoroutine = StartCoroutine(PrintPart());
        }
        else
        {
            gameObject.SetActive(false);
            playerMovement.SetMoving(true);
        }
    }

    private IEnumerator PrintPart()
    {
        textHolder.text = "";
        foreach (char character in parts[currentConversation].part[currentPart])
        {
            textHolder.text += character;
            yield return new WaitForSeconds(character == '.' || character == '!' || character == '?' ? 0.5f : 0.05f);
        }
        printCoroutine = null;
        yield return null;
    }

    public bool IsFinished()
    {
        return currentPart == parts[currentConversation].Length - 1;
    }

    public void NextConversation()
    {
        textHolder.text = "";
        if (currentConversation < staticConversatios - 1)
        {
            currentConversation++;
        }
        else
        {
            currentConversation = UnityEngine.Random.Range(staticConversatios, staticConversatios + randomConversations);
            foreach (Pair dependentLocation in dependentLocations)
            {
                if (dependentLocation.location.IsVisited())
                {
                    currentConversation = dependentLocation.index;
                    dependentLocations.Remove(dependentLocation);
                    break;
                }
            }
        }
        currentPart = -1;
    }

    public void ResetCurrentPart()
    {
        currentPart = -1;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationManager : MonoBehaviour
{
    [SerializeField]
    private int maxLightIntensity = 3;
    private float minRestRecovery = 0.2f;
    private Stack<Location> visitedLocations;

    private void Awake()
    {
        visitedLocations = new Stack<Location>();
    }

    public void MarkVisited(Location location)
    {
        visitedLocations.Push(location);
        Debug.Log("Visited location: " + location);
    }

    public void RevealVisitedLocations()
    {
        while (visitedLocations.Count > 0)
        {
            Location location = visitedLocations.Pop();
            location.SetVisited();
            foreach (Light light in location.GetComponentsInChildren<Light>())
            {
                if (light.intensity >= maxLightIntensity)
                {
                    break;
                }
                light.intensity += 1;
                location.restRecovery = minRestRecovery * light.intensity;
            }
        }
    }

    public void ResetLocations()
    {
        while (visitedLocations.Count > 0)
        {
            Location location = visitedLocations.Pop();
            location.GetConversation().ResetCurrentPart();
        }
    }
}

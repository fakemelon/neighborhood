﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{
    [SerializeField]
    private AudioSource goodSound;
    [SerializeField]
    private AudioSource badSound;

    private Coroutine goodCoroutine;
    private Coroutine badCoroutine;

    private void Awake()
    {
        goodSound.enabled = true;
        badSound.enabled = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            if (badCoroutine != null)
            {
                StopCoroutine(badCoroutine);
            }
            goodCoroutine = StartCoroutine(Switch(badSound, goodSound));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            if (goodCoroutine != null)
            {
                StopCoroutine(goodCoroutine);
            }
            badCoroutine = StartCoroutine(Switch(goodSound, badSound));
        }
    }

    private IEnumerator Switch(AudioSource from, AudioSource to)
    {
        while (to.volume < 1f && from.volume > 0f)
        {
            if (to.volume < 1f)
            {
                to.volume += 0.01f;
            }
            if (from.volume > 0f)
            {
                from.volume -= 0.01f;
            }
            yield return new WaitForSeconds(0.01f);
        }
        to.volume = 1f;
        from.volume = 0f;
    }
}

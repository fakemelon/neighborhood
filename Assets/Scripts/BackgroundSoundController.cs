﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSoundController : MonoBehaviour
{
    [SerializeField]
    private AudioSource birdsSound;
    [SerializeField]
    private AudioSource citySound;
    [SerializeField]
    private AudioSource nightSound;
    [SerializeField]
    private AudioSource dockSound;

    private void Update()
    {
        if (!birdsSound.isPlaying && !citySound.isPlaying && !nightSound.isPlaying && !dockSound.isPlaying)
        {
            int random = Random.Range(1, 1001);
            switch (random)
            {
                case 1:
                    birdsSound.Play();
                    break;
                case 2:
                    citySound.Play();
                    break;
                case 3:
                    nightSound.Play();
                    break;
            }
        }
    }
}

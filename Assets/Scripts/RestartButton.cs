﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class RestartButton : MonoBehaviour
{
    public PlayerMovement playerMovement;

    private void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(RetartGame);
    }

    void RetartGame()
    {
        playerMovement.RestartDay();
    }
}

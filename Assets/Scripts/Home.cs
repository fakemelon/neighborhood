﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Home : MonoBehaviour
{
    [SerializeField]
    private LocationManager locationManager;
    [SerializeField]
    private PlayerMovement playerMovement;

    private Button enterButton;
    private Canvas canvas;

    private void Start()
    {
        canvas = GetComponentInChildren<Canvas>();
        enterButton = canvas.GetComponentInChildren<Button>(true);
        enterButton.onClick.AddListener(locationManager.RevealVisitedLocations);
        enterButton.onClick.AddListener(Rest);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            enterButton.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            enterButton.gameObject.SetActive(false);
        }
    }

    private void Rest()
    {
        playerMovement.Rest(1f);
    }
}
